#!/usr/bin/env bash

whoami

echo "Starting Uvicorn"
uvicorn app:app --host="0.0.0.0" --port=55001 --reload
