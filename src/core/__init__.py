from .config import get_app_settings

__all__ = ["get_app_settings"]
