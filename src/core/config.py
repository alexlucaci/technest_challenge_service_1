from functools import lru_cache

from pydantic import BaseSettings

import os


class AppSettings(BaseSettings):
    computer_service_url: str = os.environ.get(
        "SERVICE_2_URL", "http://localhost:55002"
    )
    allowed_origins = [
        "http://localhost",
        "http://localhost:55003",
        "http://127.0.0.1",
        "null",
    ]


@lru_cache()
def get_app_settings() -> AppSettings:
    return AppSettings()
