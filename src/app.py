import fastapi

from fastapi.exceptions import RequestValidationError
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import JSONResponse
from starlette.exceptions import HTTPException as StarletteHTTPException
from starlette.staticfiles import StaticFiles

import api
import core
import os

APP_SETINGS = core.get_app_settings()
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

app = fastapi.FastAPI()
app.include_router(api.api_router)

app.mount("/", StaticFiles(directory=os.path.join(BASE_DIR, "static")))

app.add_middleware(
    CORSMiddleware,
    allow_origins=APP_SETINGS.allowed_origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    message = exc.errors()[0]["msg"]
    return JSONResponse({"message": message}, status_code=400)


@app.exception_handler(StarletteHTTPException)
async def http_exception_handler(request, exc):
    return JSONResponse({"message": str(exc.detail)}, status_code=exc.status_code)
