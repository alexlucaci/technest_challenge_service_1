import logging
import typing

from fastapi import HTTPException


class ValidatorException(HTTPException):
    def __init__(self, validator_name, msg="Data is invalid"):
        logger = logging.getLogger(self.__class__.__name__)
        logger.error(f"Validation error for {validator_name}, {msg}")
        super(ValidatorException, self).__init__(500, "Internal server error")


class BaseValidator:
    def __init__(self, data: dict) -> None:
        self.data = data
        self.validator_name = self.__class__.__name__

        self.__validate(data)

    def __validate(self, data: dict) -> dict:
        # Validates the data in the validator with following checks:
        # - if type of data is dict
        # - if all fields declared in validator are present in data
        # - if all key in data are fields in validator and if their type correspond
        # If checks are passing, original data is returned
        if type(data) is not dict:
            raise ValidatorException(self.validator_name)

        fields = self.__get_validator_fields()

        for field in fields:
            if field not in data:
                raise ValidatorException(self.validator_name)

        for key, value in data.items():
            if key not in fields or type(data[key]) != fields[key]:
                raise ValidatorException(self.validator_name)
        return data

    def __get_validator_fields(self) -> dict:
        # Get class fields and corresponding type annotation
        # eg: {'id': <class 'int'>}
        return typing.get_type_hints(self.__class__)
