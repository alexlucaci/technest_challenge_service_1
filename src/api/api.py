import fastapi

from api import endpoints

api_router = fastapi.APIRouter()

api_router.include_router(endpoints.process_router)
