import fastapi
from pydantic import Required

import core
import models
import services

APP_SETTINGS = core.get_app_settings()

router = fastapi.APIRouter()


class NumbersProcessor:
    def __init__(self, repo: services.Computer):
        self.repo = repo

    async def process(self, numbers: models.Integers):
        return await self.repo.compute_sum(numbers)


@router.post("/process")
async def process(numbers: models.Integers = Required):
    repo = services.Computer(url=APP_SETTINGS.computer_service_url)
    processor = NumbersProcessor(repo)
    data = await processor.process(numbers)
    return models.ValidResponse(data=data)
