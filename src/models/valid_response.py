from pydantic import BaseModel


class ValidResponse(BaseModel):
    message = "Response ok"
    data: dict
