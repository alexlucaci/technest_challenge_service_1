from typing import List
from pydantic import BaseModel, validator


class Integers(BaseModel):
    numbers: List[int]

    @validator("numbers", pre=True)
    def is_valid_list_of_integers(cls, numbers) -> List[int]:  # noqa: N805
        validation_message = "Not a valid list of integers"
        if len(numbers) < 2:
            raise ValueError(validation_message)
        for number in numbers:
            if type(number) is not int:
                raise ValueError(validation_message)
        return numbers
