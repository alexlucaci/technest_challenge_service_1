import logging
from enum import Enum
from typing import Union

import aiohttp


from fastapi import HTTPException


class HTTPServiceException(HTTPException):
    def __init__(self, error_code, error_message):
        logger = logging.getLogger(self.__class__.__name__)
        logger.error(f"Message: {error_message}. Status code: {error_code}")
        super(HTTPServiceException, self).__init__(500, "Internal server error")


class HTTPService:
    VALIDATION_ERROR_CODES = [400]
    SERVER_ERROR_CODES = [500, 503]

    class HTTPMethods(Enum):
        POST = "POST"
        GET = "GET"
        DELETE = "DELETE"
        PUT = "PUT"
        PATCH = "PATCH"

    def __init__(self, url):
        self.LOGGER = logging.getLogger(self.__class__.__name__)
        self.url = url
        self.headers: dict = {
            "Accept": "application/json",
            "Content-Type": "application/json",
        }

    async def do_post(self, endpoint: str, payload: Union[dict, list] = None) -> dict:
        response = await self._do_request(self.HTTPMethods.POST, endpoint, payload)

        return await self._handle_response(response)

    async def _do_request(
        self, method: HTTPMethods, endpoint: str, payload: Union[dict, list] = None
    ) -> aiohttp.ClientResponse:
        url = self.url + endpoint

        async with aiohttp.ClientSession() as session:
            try:
                response = await session.request(
                    method=method.value,
                    url=url,
                    headers=self.headers,
                    json=payload,
                    verify_ssl=False,
                )
            except aiohttp.ClientConnectionError:
                raise HTTPServiceException(
                    500, f"Connection to service {self.__class__} is down"
                )

        return response

    async def _handle_response(self, response: aiohttp.ClientResponse) -> dict:
        if response is None:
            raise HTTPServiceException(
                f"Something went wrong when connecting to the {self.__class__} service. Response is None.",
                503,
            )

        json_response = await response.json()
        response_message = (
            f"Status Code: {response.status}. " f"Message {json_response['message']}"
        )

        if response.status in self.VALIDATION_ERROR_CODES:
            raise HTTPException(response.status, json_response["message"])

        if response.status in self.SERVER_ERROR_CODES:
            raise HTTPException(500, "Internal server error")

        self.LOGGER.info(f"Response: {response_message}")

        return json_response
