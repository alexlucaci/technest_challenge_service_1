import validators
import models
from .http_service import HTTPService


class Computer(HTTPService):
    async def compute_sum(self, numbers: models.Integers) -> dict:
        payload = dict(numbers)
        response = await self.do_post(endpoint="/sum", payload=payload)
        # Validate that the answer from service is what we expect in terms of syntax
        response = validators.Sum(response["data"]).data
        return response
