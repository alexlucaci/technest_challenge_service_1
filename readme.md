# Assumptions & decisions

I have made the following assumption and made the following decisions based on the service/component:


# Technologies

Python3.7
Fastapi as the web framework due to its lightweight and speed
Pytest
Html + bootstrap for index.html
Docker + docker compose

# challenge.zip

Extracted as a folder **challenge** which has 2 sub folders (service1, service2), a **docker-compose.yml** 
and a **startup.sh**

# startup.sh

Uses docker-compose so it's a requirement for running the containers, --build parameter is present in docker-compose up
command to ensure latest changes are present.

Declares 2 services: 
- service1 which depends on service2 and set service2 url as env variable, exposes port 55001 to public internet
- service2 which is not exposing any port to local, but running the service app on port 55002 internally
 it's only accessible from service 1 service, not public to internet

# index.html

Is a static file located on service 1, served by uvicorn, located on http://localhost:55001/index.html

Have a form with 2 inputs and create result below the form if numbers are integers (not floats), but due to Javascript,
1.0 will be treated as 1 before it arrives in backend so will not throw an error.

Error handling is done with Toasts messages in upper right corner, dismissable with x or fades after 10 seconds

# service 1

FastApi application that uses another http computer service (service 2). Service 2 url is fetched via env variable 

Accepts post on /process and validates if payload is with format {"numbers": [1, 2]}

At least one number is required in the array to pass validation, no floats, no strings, no bools, no nothing
if validation pass, request to /sum on service 2 is passed. Validation errors from service 2 (odd numbers) are also 
propagated back to the user.

# service 2

FastApi application that is not exposed on the internet, only accessible from service 1 docker container.

Uses another http Odd Sum service (sub application of fastapi). Written in the same project, but decoupled and thus
can be decoupled to a different service.

Accepts post on /sum, validates the same as service 1 /process for integers -> passes the payload to internal /oddsum
which validates the integers to be odd, if not, error is propagated back to /sum which propagate it back to /process on
service 1
