import asyncio

import aiohttp
from unittest import mock

from starlette.testclient import TestClient
from parametrization import Parametrization as Param

from app import app


class TestProcess:
    @classmethod
    def setup_class(cls):
        cls.client = TestClient(app)

    def setup_method(self):
        self.future = asyncio.Future()

    def make_process_call(self, numbers):
        return self.client.post("/process", json=numbers)

    @mock.patch("services.Computer.do_post")
    def test_200_ok_with_validator_validation(self, mock_get):
        self.future.set_result({"message": "Response ok", "data": {"sum": 5}})
        mock_get.return_value = self.future

        numbers = {"numbers": [1, 4]}

        response = self.make_process_call(numbers)

        assert response.status_code == 200
        assert response.json() == {"message": "Response ok", "data": {"sum": 5}}

    @mock.patch("services.Computer.compute_sum")
    def test_200_ok_without_validator_validation(self, mock_get):
        self.future.set_result({"sum": 5})
        mock_get.return_value = self.future

        numbers = {"numbers": [1, 4]}

        response = self.make_process_call(numbers)

        assert response.status_code == 200
        assert response.json() == {"message": "Response ok", "data": {"sum": 5}}

    @mock.patch("services.Computer.do_post")
    @Param.parameters("computer_service_response")
    @Param.case("altered_key", {"summ": 5})
    @Param.case("list instead of dict", [5])
    @Param.case("plain response", 5)
    @Param.case("string instead of int", {"sum": "5"})
    @Param.case("float instead of int", {"sum": 5.1})
    def test_wrong_response_from_computer_service_500(
        self, mock_get, computer_service_response
    ):
        self.future.set_result(
            {"message": "Response ok", "data": computer_service_response}
        )
        mock_get.return_value = self.future

        numbers = {"numbers": [1, 4]}

        response = self.make_process_call(numbers)

        assert response.status_code == 500

    @Param.parameters("numbers")
    @Param.case("1 number", {"numbers": [1]})
    @Param.case("no number", {"numbers": []})
    @Param.case("1 number not integer_1", {"numbers": [1, ""]})
    @Param.case("1 number not integer_2", {"numbers": ["", 1]})
    @Param.case("1 number not integer_2", {"numbers": [True, 1]})
    @Param.case("1 number not integer_2", {"numbers": [1, False]})
    @Param.case("both numbers strings", {"numbers": ["", ""]})
    @Param.case("both numbers bools", {"numbers": [True, False]})
    @Param.case("both numbers dict", {"numbers": [{}, {}]})
    @Param.case("both numbers float", {"numbers": [1.1, 2.2]})
    @Param.case("1 number float, 1 int_1", {"numbers": [1, 2.2]})
    @Param.case("1 number float, 1 int_2", {"numbers": [1.1, 2]})
    @Param.case("wrong syntax_1", {"number": [1, 2]})
    @Param.case("wrong syntax_2", [1, 2])
    @Param.case("wrong syntax_3", [])
    @Param.case("Wrong syntax_4", None)
    def test_validation_on_request_400(self, numbers):
        response = self.make_process_call(numbers)

        assert response.status_code == 400

    def test_response_from_service_2_is_ok_200(self, monkeypatch):
        class AsyncMock:
            async def __call__(self, *args, **kwargs):
                return self

            async def json(self):
                return {"message": "Response ok", "data": {"sum": 5}}

        mock_response = AsyncMock()
        mock_response.status = 200
        monkeypatch.setattr(
            "services.http_service.aiohttp.ClientSession.request", mock_response
        )

        numbers = {"numbers": [1, 4]}

        response = self.make_process_call(numbers)

        assert response.status_code == 200
        assert response.json() == {"message": "Response ok", "data": {"sum": 5}}

    def test_response_from_service_2_is_500(self, monkeypatch):
        class AsyncMock:
            async def __call__(self, *args, **kwargs):
                return self

            async def json(self):
                return {"message": "error"}

        mock_response = AsyncMock()
        mock_response.status = 500
        monkeypatch.setattr(
            "services.http_service.aiohttp.ClientSession.request", mock_response
        )

        numbers = {"numbers": [1, 4]}

        response = self.make_process_call(numbers)

        assert response.status_code == 500

    def test_response_from_service_2_is_503(self, monkeypatch):
        class AsyncMock:
            async def __call__(self, *args, **kwargs):
                return self

            async def json(self):
                return {"message": "error"}

        mock_response = AsyncMock()
        mock_response.status = 503
        monkeypatch.setattr(
            "services.http_service.aiohttp.ClientSession.request", mock_response
        )

        numbers = {"numbers": [1, 4]}

        response = self.make_process_call(numbers)

        assert response.status_code == 500

    def test_response_from_service_2_is_none_500(self, monkeypatch):
        class AsyncMock:
            async def __call__(self, *args, **kwargs):
                return None

        mock_response = AsyncMock()
        monkeypatch.setattr(
            "services.http_service.aiohttp.ClientSession.request", mock_response
        )

        numbers = {"numbers": [1, 4]}

        response = self.make_process_call(numbers)

        assert response.status_code == 500

    def test_response_from_service_2_is_not_odd_400(self, monkeypatch):
        class AsyncMock:
            async def __call__(self, *args, **kwargs):
                return self

            async def json(self):
                return {"message": "error, number is not odd"}

        mock_response = AsyncMock()
        mock_response.status = 400
        monkeypatch.setattr(
            "services.http_service.aiohttp.ClientSession.request", mock_response
        )

        numbers = {"numbers": [1, 4]}

        response = self.make_process_call(numbers)

        assert response.status_code == 400

    def test_service_2_is_unreachable_500(self, monkeypatch):
        class AsyncMock:
            async def __call__(self, *args, **kwargs):
                raise aiohttp.ClientConnectionError

        mock_response = AsyncMock()
        monkeypatch.setattr(
            "services.http_service.aiohttp.ClientSession.request", mock_response
        )

        numbers = {"numbers": [1, 4]}

        response = self.make_process_call(numbers)

        assert response.status_code == 500
