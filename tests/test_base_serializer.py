import pytest

from parametrization import Parametrization as Param

import validators


class TestBaseValidator:
    @classmethod
    def setup_class(cls):
        class TestValidator(validators.BaseValidator):
            field_1: int
            field_2: str

        cls.TestValidator = TestValidator

    def test_validator_ok(self):
        data = {"field_1": 1, "field_2": "test_string"}

        assert self.TestValidator(data).data == data

    @Param.parameters("data_to_validate")
    @Param.case("fields_in_validator_but_not_in_data", {"field_1": 1})
    @Param.case("fields_in_data_but_not_in_validator", {"field_1": 1, "field_3": 3})
    @Param.case("fields_different_types_1", {"field_1": "alex", "field_2": 3})
    @Param.case("fields_different_types_2", {"field_1": "alex", "field_2": "alex"})
    @Param.case("fields_different_types_3", {"field_1": 1, "field_2": True})
    @Param.case("fields_different_types_4", {"field_1": False, "field_2": True})
    @Param.case("fields_different_types_4", {"field_1": False, "field_2": False})
    @Param.case("fields_different_types_4", {"field_1": True, "field_2": True})
    @Param.case("fields_different_types_4", {"field_1": True, "field_2": False})
    @Param.case("not dict but list_1", [1, 2])
    @Param.case("not dict but list_2", [1])
    @Param.case("not dict but list_3", [1, "3"])
    @Param.case("not dict but list_4", [1, True])
    @Param.case("not dict but list_5", [False, True])
    @Param.case("not dict but set", {1, 2})
    @Param.case("not dict but tuple", (1, 2))
    @Param.case("empty dict", {})
    @Param.case("None_1", {None})
    @Param.case("None_2", None)
    def test_validator_error(self, data_to_validate):
        with pytest.raises(validators.ValidatorException):
            self.TestValidator(data_to_validate)
